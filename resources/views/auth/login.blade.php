@extends('layouts.app')

@section('content')
<div class="container">
    <div style="margin-top:100px;" class="row justify-content-center">
        <div class="col-md-6">
            <div style="background-color:#e4eef7" class="card">
                <!-- <div style="background-color:#0d4c82 !important;text-align:center;color:#FFF;font-weight:bold;font-size:20" class="card-header">{{ __('Login') }}</div> -->

                <div class="card-body">
                  <p>New to AirMobi? <a href="/register" class="custom-btn-link">register</a></p>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
					    <label for="email">Email Adress</label>
					    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
					      </div>

                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                        </div>

                        <div class="form-group">
                                <button style="background-color:#0d4c82" type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
