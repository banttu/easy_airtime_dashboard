@extends('client.app')

@section('content')

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Edit Contact
          </div>
       <form action="/client-editMap/{{$mapping[0]->id}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px" class="form-group">
           <label style="font-weight:bold;" for="exampleInputEmail1">Phone Number</label>
           <div class="input-group">
              @foreach($mapping as $map)
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="msisdn" type="msisdn" class="form-control @error('msisdn') is-invalid @enderror" name="msisdn" value="{{$map->contact->msisdn}}" required autocomplete="msisdn" autofocus >
           <input name="contact_id" value="{{$map->contact->id}}" type="hidden">
           @endforeach
           
           </div>
        <div>

        <div class="form-group">
                <label for="group_id" style="font-weight:bold">Select Group</label>
            <div class="input-group">
               <select name="group_id" class="form-control">
               @foreach($mapping as $map)
                   <option  value="{{$map->group->id}}">{{$map->group->group_name}}</option>
                   @endforeach
                   @foreach ($groups as $group)
                   <option  value="{{$group->id}}">{{$group->group_name}}</option>
                   @endforeach
               </select> 
               </div>
        </div>

        <div class="form-group">

              <label style="font-weight:bold;" for="exampleInputEmail1">Service Provider</label>
                <div class="input-group">
                  <select name="telco" class="form-control">
                   <option  value="SAFARICOM">SAFARICOM</option>
                   <option  value="AIRTEL">AIRTEL</option>
                 </select> 
               </div>

           <label style="font-weight:bold;" for="exampleInputEmail1">Amount</label>
           <div class="input-group">
           @foreach($mapping as $map)
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="amount"  class="form-control @error('name') is-invalid @enderror" name="amount" value="{{$map->amount}}" required autocomplete="amount" autofocus>
           @endforeach
           </div>
        <div>

     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection