<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>AirMobi</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('/images/favicon.png') }}">
     
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
          
    <nav style="background-color:#0d4c82 !important" class="navbar main-nav border-less fixed-top navbar-expand-lg p-0">
  <div class="container-fluid p-0">
      <!-- logo -->
      <a class="navbar-brand" href="/client">
        <img style="height:40px;width:40px;margin-left:30px" src="{{ asset('/images/favicon.png') }}" alt="logo">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item">
            <a style="font-size:15px;font-weight:400;color:#FFF" class="navbar-brand" href="{{ url('/client') }}">
                Contacts
            </a>
        </li>

         <li class="nav-item">
            <a style="font-size:15px;font-weight:400;color:#FFF" class="navbar-brand" href="{{ url('/client-groups') }}">
                Groups
            </a>
        </li>
         
         <li class="nav-item">
            <a style="font-size:15px;font-weight:400;color:#FFF" class="navbar-brand" href="{{ url('/client-transactions') }}">
                Airtime Top-Ups 
            </a>
        </li>
         
         <li class="nav-item">
            <a style="font-size:15px;font-weight:400;color:#FFF" class="navbar-brand" href="{{ url('/client-payments') }}">
                Payments
            </a>
        </li>
        
      </ul>

       

        <li style="background-color:#2b2a2a;padding: 12px 11px;display: block;border: 1px solid transparent;" class="nav-item dropdown">
            <a style="font-weight:bold;color:#FFF" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{$client->name}} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a style="color:#000 !important" class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
         </li>
      </div>
  </div>
</nav>
     <div style="min-height:100vh" class="row">

     <div style="margin-top:80px;margin-left:10px;" class="col-md">
        <ul class="list-group">
            <li class="list-group-item">Cras justo odio</li>
            <li class="list-group-item">Dapibus ac facilisis in</li>
            <li class="list-group-item">Morbi leo risus</li>
            <li class="list-group-item">Porta ac consectetur ac</li>
            <li class="list-group-item">Vestibulum at eros</li>
        </ul>
     </div>

     <main style="margin-top:22px;" class="col-md-10">
            @yield('content')
        </main>

     </div>
    </div>
</body>
</html>
