@extends('client.app')

@section('content')

@if ($message=Session::get('success'))
<div class="alert alert-success">
    <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <strong>{{$message}}</strong>
  </div>
@endif


<div class="row justify-content-center" style="margin-top:20px;padding:10px">

<div style="color:#0d4c82;font-weight:bold;justify-content:space-between;width:100%;align-self:center;margin-top:20px" class="row justify-content-center" style="margin-top:0px;padding:10px">
     <h3><span style="color:black;font-weght:bold">Edit Schedule for :</span>{{{$schedule->groups->group_name}}}</h3>
    </div>

      <div class="col-md-10">
      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

      @if ($message=Session::get('error'))
      <div class="alert alert-danger">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Edit Schedule
          </div>
       <form action="/updateschedule/{{$schedule->id}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px;" class="form-group">
          <div class="form-row">
              <div class="col">
                <label style="font-weight:bold;padding:5px" for="exampleInputEmail1">Interval Value</label>
                 <div class="input-group">
                     <input  id="interval_value	" type="interval_value	" class="form-control @error('name') is-invalid @enderror" name="interval_value" value="{{$schedule->interval_value}}" placeholder="e.g 10, 20, 30 ,40, e.t.c" required autocomplete="interval_value" autofocus>
                 </div>
              </div>
              <div class="col">
                <label style="font-weight:bold;padding:5px" for="exampleInputEmail1">Interval Field</label>
                <div class="input-group">
                  <select name="interval_field" class="form-control">
                  <option  value="{{$schedule->interval_field}}">{{$schedule->interval_field}}(s)</option>
                   <option  value="DAY">DAY(s)</option>
                   <option  value="WEEK">WEEK(s)</option>
                   <option  value="MONTH">MONTH(s)</option>
                   <option  value="YEAR">YEAR(s)</option>
                 </select> 
               </div>
              </div>
            </div>
              </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
        </div>
      </div>
      </div>
    </div>
@endsection