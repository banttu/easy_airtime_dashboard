@extends('client.app')

@section('content')


            <div class="row justify-content-center" style="margin-top:50px;padding:10px">

             <!-- The Modal -->
                <div class="modal" id="myModal">
                  <div class="modal-dialog">
                    <div class="modal-content">
                    
                      <!-- Modal Header -->
                      <div class="modal-header">
                        <h4 class="modal-title">
                          <strong> Topup Via</strong>
                          <span><img src="https://seeklogo.com/images/M/mpesa-logo-AE44B6F8EB-seeklogo.com.png" style="height:50px;width:100px"/></span>
                        </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      
                      <!-- Modal body -->
                      <div class="modal-body">
                        <ol>
                          <li>Go to your Mpesa Menu</li>
                          <li>Select Lipa na Mpesa</li>
                          <li>Select Buy Goods and Services</li>
                          <li><span>Enter Till Number as </span>&emsp;<strong>123456</strong></li>
                          <li>Enter Amount</li>
                          <li>Enter your Mpesa PIN</li>
                      </ol>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal -->

            <div class="col-md-10">
                  <div class="card-header-tab card-header">
                    <div class="btn-actions-pane-right text-capitalize">
                     <div class="row">
                       <div class="col">
                        <a data-toggle="modal" data-target="#myModal"  style="color:#FFF;font-size: 15px;background-color:#0d4c82;border-radius:20px" class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm">
                            <span>
                              <i style="color:#FFF;font-size: 20px;" class="fa fa-plus"></i>
                            </span>
                            Top Up
                        </a>
                      </div>
                    </div>
                    </div>
                  </div>
                      <div style="background-color:#FFF" class="no-gutters row">
                          <div class="col-sm">
                              <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                  <div class="icon-wrapper rounded-circle">
                                      <div class="icon-wrapper-bg opacity-10 bg-warning">
                                      <i style="margin-top: -5px;margin-left:20px;color:#000;font-size: 30px; color: #FFF;" class="fa fa-dollar"></i>
                                      </div>
                                      </div>
                                      <div class="widget-chart-content">
                                      <div style="margin-top:20px;margin-left:5px" class="widget-subheading"><h5><strong>Airtime Balance</strong></h5></div>
                                      <div style="margin-left:5px;margin-top:-5px" class="widget-numbers text-success"><span>Kshs.{{$balance->balance}}</span></div>
                                  </div>
                              </div>
                              <div class="divider m-0 d-md-none d-sm-block"></div>
                          </div>
                          <div class="col-sm">
                              <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                  <div class="icon-wrapper rounded-circle">
                                      <div class="icon-wrapper-bg opacity-9 bg-danger">
                                      <i style="margin-top: -2px;margin-left:17px;color:#000;font-size: 25px; color: #FFF;" class="fa fa-group"></i>
                                      </div>
                                      <i class="lnr-graduation-hat text-white"></i></div>
                                  <div class="widget-chart-content">
                                      <div style="margin-top:20px;margin-left:5px" class="widget-subheading"><h5><strong>Groups</strong></h5></div>
                                      <div style="margin-left:5px;margin-top:-5px" class="widget-numbers text-success"><span>{{$groupCount}}</span></div>
                                  </div>
                              </div>
                              <div class="divider m-0 d-md-none d-sm-block"></div>
                          </div>

                          <div class="col-sm">
                              <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                  <div class="icon-wrapper rounded-circle">
                                      <div class="icon-wrapper-bg opacity-9 bg-success">
                                      <i style="margin-top: -2px;margin-left:17px;color:#000;font-size: 25px; color: #FFF;" class="fa fa-address-book"></i>
                                      </div>
                                      <i class="lnr-apartment text-white"></i></div>
                                  <div class="widget-chart-content">
                                      <div style="margin-top:20px;margin-left:5px" class="widget-subheading"><h5><strong>Contacts</strong></h5></div>
                                      <div style="margin-left:5px;margin-top:-5px" class="widget-numbers text-success"><span>{{$contactCount}}</span></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="text-center d-block p-3 card-footer">
                      </div>
                  </div>

            </div>



      <div class="row justify-content-center" style="margin-top:5px;padding:10px">
      <div class="col-md-10">
      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

      @if ($message=Session::get('error'))
      <div class="alert alert-danger">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Add Contact
          </div>
       <form action="/client-add-contact" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px;" class="form-group">
          <div class="form-row">
              <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1">Phone No</label>
                 <div class="input-group">
                     <input  id="msisdn" type="msisdn" class="form-control @error('name') is-invalid @enderror" name="msisdn" value="{{ old('name') }}" placeholder="07XXXXXXXX" required autocomplete="msisdn" autofocus>
                 </div>
              </div>
              <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1">Service Provider</label>
                <div class="input-group">
                  <select name="telco" class="form-control">
                   <option  value="SAFARICOM">SAFARICOM</option>
                   <option  value="AIRTEL">AIRTEL</option>
                 </select> 
               </div>
              </div>
            </div>

            <div class="form-row">
            <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1">Select Group</label>
                <div class="input-group">
                  <select name="group_id" class="form-control">
                  @foreach($groups as $group)
                      <option  value="{{$group->id}}">{{$group->group_name}}</option>
                    @endforeach
                 </select> 
               </div>
              </div>
              <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1">Amount</label>
                 <div class="input-group">
                     <input  id="amount" type="amount" class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}" placeholder="" required autocomplete="amount" autofocus>
                 </div>
              </div>
            </div>
              </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>

    <div class="row justify-content-center" style="margin-top:5px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;padding:10px;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
            Contacts
              <!-- <a style="background-color:#0d4c82;border-color:#0d4c82" class="btn btn-primary">    </a> -->
          </div>
         <div>
          <table id="datatable"  class="table table-striped table-bordered">
          <thead>
              <tr>
                <th class="blue-txt" scope="col">No.</th>
                <th class="blue-txt" scope="col">Group Name</th>
                <th class="blue-txt" scope="col">Phone No</th>
                <th class="blue-txt" scope="col">Service Provider</th>
                <th class="blue-txt" scope="col">Amount</th>
                <th class="blue-txt"  scope="col">Edit</th>
              </tr>
            </thead>
                <tbody>
                <?php $index = 0;?>
                @foreach ($mappings as $mapping)
                  <tr>
                    <th scope="row" height="5">{{$index = $index+1}}.</th>
                    <td>{{$mapping->group->group_name}}</td>
                    <td>{{$mapping->contact->msisdn}}</td>
                    <td>{{$mapping->contact->telco}}</td>
                    <td>{{$mapping->amount}}</td>
                    <td style="text-align: center !mportant;">
                        <a href="/client-map-edit/{{$mapping->id}}" style="background-color:#0d4c82;border-radius: 5px;padding:5px;color:#FFF;" class="fa fa-edit"></a>
                    </td>
                    <!-- <td>
                      <a href="/role-delete/{{$group->id}}" style="height:20px;background-color:#d91414;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-trash-o" >Delete</a>
                    </td> -->
                  </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      </div>
    </div>
    </div>

    

@endsection