@extends('client.app')

@section('content')

<div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#19869c;color:white;font-weight:bold" class="card-header">
              Edit Post
          </div>
       <form action="/update/{{$post->id}}" enctype="multipart/form-data" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
        <div class="md-form mb-6">

             <div class="form-group">
                <label for="category" style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px;font-weight:bold">Post Category</label>
                
               <div class="input-group">
               <select name="category_id" class="form-control" style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px">
                  @foreach ($categories as $category)
                         <option  value="{{$category->id}}">
                         {{$category->name}}
                         </option>
                   @endforeach
               </select> 
               </div>
        </div>

        <div class="form-group">
                <label for="title" style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px;font-weight:bold">Post Title</label>
                
               <div class="input-group">
               <input style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px" id="title" type="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{$post->title}}" required autocomplete="title" autofocus>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter category name"> -->
               </div>
        </div>

             <div class="form-group">
                <label for="description" style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px;font-weight:bold">Post Description</label>
                
               <div class="input-group">
               <textarea name="description" id="article-ckeditor"  class="form-control rounded @error('description') is-invalid @enderror" rows="10" name="description" value="{{$post->description}}" required autocomplete="description" autofocus style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px">{{$post->description}}</textarea>
                                    
                                    @error('description')
                                     <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                            </span>
                                 @enderror
                    </div>
                 <div>

                 <div class="col-sm-offset-2 col-sm-10" style="margin-left: 10px;margin-top: 5px;margin-bottom: 5px;align-self:center">
                    <label class="file-upload">
                         Browse for file ... <input type="file" class="form-control rounded @error('image') is-invalid @enderror" name="image" />
                     </label>
                     @error('image')
                                     <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                            </span>
                                         @enderror
                 </div>
            </div>
            <button style="margin-top: 29px;background-color:#19869c;margin-bottom:29px;margin-left:20px" type="submit" class="btn btn-success">
                     Update Post
            </button> 
         </div>
     </form>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection