@extends('client.app')

@section('content')

      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

    <div class="row justify-content-center" style="margin-top:20px">
			<div class="col-md-10">
				<div class="card">
                 <div style="background-color:#0d4c82;color:#FFF;text-align:center" class="card-header">

                        <h5>Add User</h5>

                    </div>
				  <div class="card-body">

                    <form action="/create-user" method="POST">
                     {{csrf_field()}}
                        {{method_field('POST')}}
                    
                    <div class="form-group">
					    <label for="name">Full Name</label>
					    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>


                      <div class="form-group">
                            <label for="phone">{{ __('Phone Number') }}</label>
                                <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="07XXXXXXXX" minlength="10" maxlength="10" required autocomplete="phone">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>


                        <div class="form-group">
					    <label for='company_name'>Company Name</label>
					    <input id='company_name' type="text" class="form-control @error('company_name') is-invalid @enderror" name='company_name' value="{{ old('company_name') }}" required autocomplete='company_name' autofocus>

                                @error('company_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>


                      <div class="form-group">
					    <label for="phone">Email Adress</label>
					    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
					  </div>
                      
				      
					  <div class="form-group">
					    <label for="exampleInputPassword1">Password</label>
                        
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        
                      </div>
                      
                      <div class="form-group">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
					  
                        <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                         Submit
                       </button>
					</form>
				  </div>
				</div>
			</div>
		</div>

@endsection