@extends('client.app')

@section('content')

@if ($message=Session::get('success'))
<div class="alert alert-success">
    <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <strong>{{$message}}</strong>
  </div>
@endif


<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

      @if ($message=Session::get('error'))
      <div class="alert alert-danger">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif
     
      </div>
    </div>

    <div class="row justify-content-center" style="margin-top:0px;">

    
    <div style="color:#0d4c82;font-weight:bold;justify-content:space-between;width:100%;align-self:center;margin-top:20px" class="row justify-content-center" style="margin-top:0px;padding:10px">
     <h3><span style="color:black;font-weght:bold">Group Name :</span>{{{$group->group_name}}}</h3>
    </div>

<div class="col-md-10">
<div style="margin-top:20px;border-color:#FFF"  class="card">
    <div style="background-color:#0d4c82;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
        Available contacts 
        <a href="/addContact/{{$group->id}}" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Add contacts</a>
        <!-- <a href="/addContact/{{$group->id}}" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Airtime Transactions</a> -->
      </div>
      <div style="overflow-x:auto;">
    <table class="table table-bordered">
<thead>
<tr>
<th scope="col">No.</th>
<th scope="col">Phone No</th>
<th scope="col">Service Provider</th>
<th scope="col">Status</th>
<th class="text-center"  scope="col">Edit</th>
</tr>
</thead>
<tbody>
<?php $index = 0;?>
@foreach ($contacts as $contact)
<tr>
<th scope="row">{{ $index = $index + 1}} .</th>
<td>{{$contact->contacts[0]->msisdn}}</td>
<td>{{$contact->contacts[0]->telco}}</td>
<td>{{$contact->contacts[0]->status}}</td>
<td class="text-center"> 
    <a href="/client-editcontact/{{$contact->contacts[0]->id}}"  style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-eye">Edit</a>
    <!-- <a href="/deletecontact/{{$contact->id}}" style="height:20px;background-color:#d91414;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-trash-o">Delete</a> -->
</td>
</tr>
    @endforeach
    </tbody>
  </table>
  </div>
        </div>
        </div>
      </div>

      <div class="row justify-content-center" style="margin-top:0px;">
      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
            Available schedules
              <a href="/client-schedules-add/{{$group->id}}" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Create Schedule</a>
          </div>
          <div style="overflow-x:auto;">
          <table class="table table-bordered">
      <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Group Name</th>
      <th scope="col">Interval Value</th>
      <th scope="col">Interval Field</th>
      <th scope="col">Status</th>
      <th  scope="col">Last Executed</th>
      <th  scope="col">Next Run</th>
      <th  scope="col">Starts At</th>
      <th  scope="col">Ends At</th>
      <th class="text-center" scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>
  <?php $index = 0;?>
  @foreach ($schedules as $schedules)
    <tr>
      <th scope="row">{{ $index = $index + 1}} .</th>
      <td>{{$schedules->groups->group_name}}</td>
      <td>{{$schedules->interval_value}}</td>
      <td>{{$schedules->interval_field}}(s)</td>
      <td>{{$schedules->status}}</td>
      <td>{{date('M d'.', '.'Y', strtotime($schedules->last_executed))}}</td>
      <td>{{date('M d'.', '.'Y', strtotime($schedules->next_run))}}</td>
      <td> {{date('M d'.', '.'Y', strtotime($schedules->starts_at))}} </td>
      <td>{{date('M d'.', '.'Y', strtotime($schedules->ends_at))}}</td>
    
      <td class="text-center">
          <a href="editschedules/{{$schedules->id}}"  style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-edit">Edit</a>
          <!-- <a href="/deleteschedules/{{$schedules->id}}" style="height:20px;background-color:#d91414;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-trash-o">Delete</a> -->
      </td>
    </tr>
          @endforeach
          </tbody>
        </table>
        </div>
              </div>
              </div>
            </div>

           

            </div>

@endsection