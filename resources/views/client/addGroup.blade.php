@extends('client.app')

@section('content')

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
           {{$clients->name}} : Add Group
          </div>
       <form action="/client-add-group" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-bottom:10px;margin-right:10px ">
           <label style="font-weight:bold;" for="exampleInputEmail1">Group Name</label>
           <div class="input-group">
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="group_name" type="group_name" class="form-control @error('group_name') is-invalid @enderror" name="group_name" value="{{ old('group_name') }}" required autocomplete="group_name" autofocus>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter client name"> -->
           </div>
        <div>

        <input hidden name="client_id" value="{{$clients->id}}"></input>

     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection