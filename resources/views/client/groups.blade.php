@extends('client.app')

@section('content')
  <div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      @if ($message=Session::get('success'))
      <div class="alert alert-success">
          <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
          <strong>{{$message}}</strong>
        </div>
      @endif

      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
            Add Group
          </div>
       <form action="/client-add-group" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-bottom:10px;margin-right:10px ">
           <label style="font-weight:bold;" for="exampleInputEmail1">Group Name</label>
           <div class="input-group">
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="group_name" type="group_name" class="form-control @error('group_name') is-invalid @enderror" name="group_name" value="{{ old('group_name') }}" required autocomplete="group_name" autofocus>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter client name"> -->
           </div>
        <div>

     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

      <div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
              Contact Groups
              <!-- <a href="/client-group-add" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Add group</a> -->
          </div>
          <table class="table table-bordered">
      <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Group Name</th>
      <th scope="col">Client Name</th>
      <th scope="col">Status</th>
      <th class="text-center" scope="col">View</th>
      <th class="text-center" scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>
  <?php $index = 0;?>
  @foreach ($groups as $group)
    <tr>
      <th scope="row">{{ $index = $index + 1}} .</th>
      <td>{{$group->group_name}}</td>
      <td>{{$group->client->name}}</td>
      <td>{{$group->status}}</td>
      <td class="text-center">
        <a href="/client-group/{{$group->id}}"  style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-eye">View</a>
       </td>
      <!-- <td><a href="/client-viewgroup/{{$group->id}}" style="height:20px;background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-eye">View</a></td> -->
      <td class="text-center">
          <a href="client-editgroup/{{$group->id}}"  style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-edit">Edit</a>
      </td>
    </tr>
          @endforeach
          </tbody>
        </table>
              </div>
              </div>
            </div>
            </div>

@endsection