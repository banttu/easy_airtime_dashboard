@extends('client.app')

@section('content')

      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

    <div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;padding:5px;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
                All transactions
          </div>
         <div table-responsive>
          <table class="table table-bordered">
          <thead>
              <tr>
                <th class="blue-txt" scope="col">No.</th>
                <th class="blue-txt" scope="col">Phone No</th>
                <th class="blue-txt" scope="col">Group</th>
                <th class="blue-txt" scope="col">Amount</th>
                <th class="blue-txt"  scope="col">Disbursed On:</th>
              </tr>
            </thead>
                <tbody>
                <?php $index = 0;?>
                @foreach ($transactions as $transaction)
                  <tr>
                    <th scope="row" height="5">{{$index = $index+1}}.</th>
                    <td>{{$transaction->msisdn}}</td>
                    <td>{{$transaction->group_name}}</td>
                    <td>{{$transaction->amount}}</td>
                    <td>{{date('M d'.', '.'Y', strtotime($transaction->created_at))}}</td>
                  </tr>
             @endforeach
          </tbody>
        </table>
      </div>
      </div>
    </div>
    </div>

@endsection