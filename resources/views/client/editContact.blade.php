@extends('client.app')

@section('content')


@if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Edit Contact
          </div>
       <form action="/client-update-contact/{{$contact->id}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-bottom:10px;margin-right:10px ">
           <label style="font-weight:bold;" for="exampleInputEmail1">Phone Number</label>
           <div class="input-group">
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="msisdn" type="msisdn" class="form-control @error('msisdn') is-invalid @enderror" name="msisdn" value=" {{$contact->msisdn}}" required autocomplete="msisdn" autofocus>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter client name"> -->
           </div>
        <div>


        <div class="form-group">
                <label for="status" style="font-weight:bold">Update Status</label>
            <div class="input-group">
               <select name="status" class="form-control">
                   <option  value="ACTIVE">ACTIVE</option>
                   <option  value="INACTIVE">INACTIVE</option>
                   <option  value="SUSPENDED">SUSPENDED</option>
                   <option  value="DELETED">DELETED</option>
               </select> 
               </div>
        </div>

     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection