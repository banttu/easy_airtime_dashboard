@extends('client.app')

@section('content')


<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

      @if ($message=Session::get('error'))
      <div class="alert alert-danger">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif
      <div style="margin-top:20px"  class="card">
          <div style="color:#0d4c82;font-weight:bold;justify-content:space-between;width:100%;align-self:center;margin-top:20px" class="row justify-content-center" style="margin-top:0px;padding:10px">
            <h5><span style="color:black;font-weght:bold">Add Contacts to :</span>{{{$group->group_name}}}<span style="color:black;font-weght:bold"> by Uploading a CSV file or Adding contacts One by one.</span></h5>
          </div>
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Upload Contact CSV
          </div>
          
          <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                @csrf
          <div style="margin-left:10px;margin-right:10px;" class="form-group">
            <div class="form-row">
              <div class="col-sm-2">
                <label style="font-weight:bold;" for="exampleInputEmail1">Sample CSV file</label>
                <div class="input-group">
                  <a href="/contacts/download/sample.csv">Click here to download</a>
                 </div>
              </div>
              <div class="col-sm-2">
                <label style="font-weight:bold;" for="exampleInputEmail1">Upload CSV file</label>
                 <div class="input-group">
                   <input type="file" name="file" accept=".csv">
                    <input type='hidden' name="group_id" value="{{$group->id}}">
                  </div>
              </div>
              <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1"></label>
                <div class="input-group">
                  <button type="submit" style="background-color:#0d4c82;" class="btn btn-primary">Upload</button>
               </div>
               </form>
            </div>
          </div>
      </div>
      </div>
      </div>
      </div>
    <div class="row justify-content-center" style="margin-top:5px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Add Contact
          </div>
       <form action="/client-add-contact" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px;" class="form-group">
           <label style="font-weight:bold;" for="exampleInputEmail1">Phone No</label>
           <div class="input-group">
           <input type='hidden' name="group_id" value="{{$group->id}}">
           <input  id="msisdn" type="msisdn" class="form-control @error('name') is-invalid @enderror" name="msisdn" value="{{ old('name') }}" placeholder="07XXXXXXXX" required autocomplete="msisdn" autofocus>
           </div>

           <label style="font-weight:bold;" for="exampleInputEmail1">Service Provider</label>
           <div class="input-group">
               <select name="telco" class="form-control">
                   <option  value="SAFARICOM">SAFARICOM</option>
                   <option  value="AIRTEL">AIRTEL</option>
               </select> 
               </div>

               <label style="font-weight:bold;" for="exampleInputEmail1">Amount</label>
               <div class="input-group">
                  <input  id="amount" type="amount" class="form-control @error('name') is-invalid @enderror" name="amount" value="{{ old('name') }}" placeholder="" required autocomplete="amount" autofocus>
               </div>
        <div>
     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection