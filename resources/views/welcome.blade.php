@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div  class="container">

<!-- Heading Row -->
<div class="row align-items-center my-5">
  <div class="col-lg-6">
    <img src="{{url('images/people_on_phone.png')}}" alt="" class="img-fluid">
  </div>
  <!-- /.col-lg-8 -->
  <div class="col-md-6">
     <!-- <div style="background-color:#edebeb;"  class="card">
        <h5 class="simple-headeng"><strong>Simplify your Organization's Airtime Management<strong></h5>
        <p  class="card-info">AirMobi simplifies your airtime management as an organization by providing automated topups, group management and airtime quota management.</p>
       <a href="{{ route('register') }}">
         <button class="btn btn-primary card-button" style="background-color: #0d4c82 !important;width:200px">Register</button>
        </a>  
    </div> -->
    <div class="container" style="padding:10px;">
            <div class="row align-items-center">
                <div class="card">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                        <h5 class="simple-headeng"><strong>Simplify your Organization's Airtime Management<strong></h5>
                            <p class="card-info">
                            AirMobi simplifies your airtime management as an organization by providing automated topups, group management and airtime quota management.
                            </p>
                            <a href="{{ route('register') }}" style="background-color: #0d4c82 !important;width:120px; margin-left:10px !important;margin-bottom:20px ;height:42px" class="btn_1">Register</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
  <!-- /.col-md-4 -->
</div>
<!-- /.row -->

<!-- Call to Action Well -->

    <div class="section_tittle text-center">
                        <h2><strong>Smart Airtime Service </strong> </h2>
                        <!-- <hr class="line2"/> -->
     </div>


<!-- feature_part start-->
<section class="feature_part">
        <div class="container">
            <div class="row">
                <div class="single_feature col-sm-6 col-xl-3 align-self-center">
                <div class="single_feature">
                        <div class="single_feature_part">
                          <img class="card-img" src="{{url('images/safaricom_cards.png')}}" style="width:100px;margin:10px">
                            <h4>Affordable Airtime</h4>
                            <p>
                            AirMobi allows you to access discounted airtime for your employees, stakeholders and members of your organization
                            </p>
                        </div>
                    </div>
                </div>
                <div class="single_feature col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                        <img class="card-img" src="{{url('images/convenience.png')}}" style="width:100px;margin:10px">
                            <h4>Available and Convenient</h4>
                            <p>
                            AirMobi airtime is available always when you need it. Load your account and consume at your pace. That's convenient!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                        <img class="card-img" src="{{url('images/quota.png')}}" style="width:100px;margin:10px">
                            <h4>Simple Quota Management</h4>
                            <p>
                            Manage your airtime quotas for the different levels of your employees; executive, managerial, sales etc.
                            </p>
                        </div>
                    </div>
                </div>
                <div  class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part single_feature_part_2">
                        <img class="card-img" src="{{url('images/safaricom_cards.png')}}" style="width:100px;margin:10px">
                            <h4>
                                <h4>Automated Disbursements</h4>
                            <p>
                            Easily set when the airtime beneficiaries receive their allocation and that's all. The system will do the junk work for you!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcoming_event part start-->
    
    <div class="about text-center">
    <h2 style="color: #000; !important;margin-bottom:30px"><strong>About us</strong> </h2>
    <!-- <hr class="line"/> -->
     </div>
    <div class="row">
            <p style="line-height: 2.2em;margin-left:10px;margin-right:10px">   
                AirMobi is an automated global airtime distributor. 
                The system automates airtime topup directly to your phone. You simply create an account with your organization, add airtime recipients, allocate them quotas and schedule when they receive the topups

                The system will not only automate this disbursements but also keep an accurate account of every transaction which is available to you at your convenience. 

                
                Why choose AirMobi?
                As captured above, AirMobi provides you with a lot of benefits as an individual or an organization. Key benefits include:
            <ul style="line-height: 2.2em;">
                    <li><span style="color: #337AB7;font-weight: bold;">Simple and easy Setup:</span> You just register and that's all.</li>
                    <li><span style="color: #337AB7;font-weight: bold;">Affordable Airtime:</span> We discount our airtime for you to save you that very important penny</li>
                    <li><span style="color: #337AB7;font-weight: bold;">Available and Convenient:</span> Its available when you need it. Access anywhere and load your account anytime</li>
                    <li><span style="color: #337AB7;font-weight: bold;">Automated Disbursements:</span> Why must you get bogged up by mundane tasks like remembering to disburse airtime to the recipients in your organization. Just set it up once and that's all, the system does the rest. Focus on what matters in your business!</li>
                </ul>
 
            </p>
        </div>	

</div>
<!-- /.container -->


@endsection
