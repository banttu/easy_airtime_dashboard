@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div  class="container">

<!-- Heading Row -->
<div class="row align-items-center my-5">
  <div class="col-lg-6">
    <img src="{{url('images/people_on_phone.png')}}" alt="" class="img-fluid">
  </div>
  <!-- /.col-lg-8 -->
  <div class="col-md-6">
     <div style="background-color:#edebeb;"  class="card">
        <h3 class="simple-headeng"><strong>Simplify your Organization's Airtime Management<strong></h3>
        <p  class="card-info">AirMobi simplifies your airtime management as an organization by providing automated topups, group management and airtime quota management.</p>
        <button class="btn btn-primary card-button" style="background-color: #337ab7 !important;width:200px">Register</button>
    </div>
  </div>
  <!-- /.col-md-4 -->
</div>
<!-- /.row -->

<!-- Call to Action Well -->
<div class="card text-white bg-dark my-5 py-4 text-center">
  <header class="section-header">
          <h3><span>Air</span><span style="color:#337ab7">Mobi</span> <span>  - Smart Airtime Service </span> </h3>
        </header>
</div>

<!-- Content Row -->
<div class="row" style="padding-bottom:50px !important" >
            <div  class="col-md-3 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="service_box hi-icon-effect-3 hi-icon-effect-3b shadow-lg " style="margin-top: 5px;min-height: 300px !important;max-height: 400px !important;">
                        <div class="centered-content">
                            <div  style="margin-top:20px" class="service-icon centered-content">
                                <span class="card-img"><img src="{{url('images/safaricom_cards.png')}}" width="80%"></span>
                            </div>
                            <div style="margin-top:20px !important" class="service_details">
                                <p class="card-title"><strong>Affordable Airtime</strong></p>
                                <p style="font-size:14px" class="small normal-color-txt">
                                    AirMobi allows you to access discounted airtime for your employees, stakeholders and members of your organization
                                </p>
                            </div>
                        </div>

                    </div>
            </div>

            <div class="col-md-3">
                <div class="service_box hi-icon-effect-3 hi-icon-effect-3b shadow-lg " style="margin-top: 5px;min-height: 300px !important;max-height: 400px !important;">
                    <div class="centered-content">
                        <div class="service-icon centered-content">
                            <span class="card-img"><img src="{{url('images/convenience.png')}}" width="80%" ></span>
                        </div>

                        <div class="service_details">
                            <p class="card-title"><strong>Available and Convenient</strong></p>
                            <p style="font-size:14px" class="small normal-color-txt">
                                AirMobi airtime is available always when you need it. Load your account and consume at your pace. That's convenient!
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-3">
                    <div class="service_box hi-icon-effect-3 hi-icon-effect-3b shadow-lg " style="margin-top: 5px;min-height: 300px !important;max-height: 400px !important;">
                        <div class="centered-content">
                            <div class="centered-content">
                        <div class="service-icon centered-content">
                            <span class="card-img"><img src="{{url('images/quota.png')}}" width="80%" height="50%"></span>
                        </div>

                        <div class="service_details">
                            <p class="card-title"><strong>Simple Quota Management</strong></p>
                            <p style="font-size:14px" class="small normal-color-txt">
                                Manage your airtime quotas for the different levels of your employees; executive, managerial, sales etc.
                            </p>
                            </div>
                        </div>

                    </div>
                    
            </div>
        </div>

             <div class="col-md-3  col-lg-3 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="service_box hi-icon-effect-3 hi-icon-effect-3b shadow-lg  " style="margin-top: 5px;min-height: 300px !important;max-height: 400px !important;">
                        <div class="centered-content">
                            <div class="service-icon centered-content">
                            <span class="card-img"><img src="{{url('images/disbursement.png')}}" width="80%" height="50%"></span>
                        </div>

                        <div class="service_details">
                            <p class="card-title"><strong>Automated Disbursements</strong></p>
                            <p style="font-size:14px" class="small normal-color-txt">
                                Easily set when the airtime beneficiaries receive their allocation and that's all. The system will do the junk work for you!
                            </p>
                            </div>
                        </div>

                    </div>
            </div>
        
      </div>
</div>
<!-- /.row -->

</div>
<!-- /.container -->
@endsection
