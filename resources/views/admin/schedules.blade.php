@extends('admin.app')

@section('content')

@if ($message=Session::get('success'))
<div class="alert alert-success">
    <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <strong>{{$message}}</strong>
  </div>
@endif


<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

      @if ($message=Session::get('error'))
      <div class="alert alert-danger">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Add Schedule
          </div>
       <form action="/schedulesave/{{$group->id}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px;" class="form-group">
          <div class="form-row">
              <div class="col">
                <label style="font-weight:bold;padding:5px" for="exampleInputEmail1">Interval Value</label>
                 <div class="input-group">
                     <input  id="interval_value	" type="interval_value	" class="form-control @error('name') is-invalid @enderror" name="interval_value" value="{{ old('interval_value') }}" placeholder="e.g 10, 20, 30 ,40, e.t.c" required autocomplete="interval_value" autofocus>
                 </div>
              </div>
              <div class="col">
                <label style="font-weight:bold;padding:5px" for="exampleInputEmail1">Interval Field</label>
                <div class="input-group">
                  <select name="interval_field" class="form-control">
                   <option  value="DAY">DAY</option>
                   <option  value="WEEK">WEEK</option>
                   <option  value="MONTH">MONTH</option>
                   <option  value="YEAR">YEAR</option>
                 </select> 
               </div>
              </div>
            </div>
              </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>

      <div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
            {{{$group->group_name}}} : Available schedules
              <!-- <a href="/schedules-add" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Create Schedule</a> -->
          </div>
          <table class="table table-bordered">
      <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Group Name</th>
      <th scope="col">Interval Value</th>
      <th scope="col">Interval Field</th>
      <th scope="col">Status</th>
      <th  scope="col">Last Executed</th>
      <th  scope="col">Next Run</th>
      <th  scope="col">Starts At</th>
      <th  scope="col">Ends At</th>
      <th  scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>
  <?php $index = 0;?>
  @foreach ($schedules as $schedules)
    <tr>
      <th scope="row">{{ $index = $index + 1}} .</th>
      <td>{{$schedules->groups->group_name}}</td>
      <td>{{$schedules->interval_value}}</td>
      <td>{{$schedules->interval_field}}(s)</td>
      <td>{{$schedules->status}}</td>
      <td>{{date('M d'.', '.'Y', strtotime($schedules->last_executed))}}</td>
      <td>{{date('M d'.', '.'Y', strtotime($schedules->next_run))}}</td>
      <td> {{date('M d'.', '.'Y', strtotime($schedules->starts_at))}} </td>
      <td>{{$schedules->ends_at}}</td>
    
      <td>
          <a href="/schedulesedit/{{$schedules->id}}"  style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-edit">Edit</a>
          <!-- <a href="/deleteschedules/{{$schedules->id}}" style="height:20px;background-color:#d91414;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-trash-o">Delete</a> -->
      </td>
    </tr>
          @endforeach
          </tbody>
        </table>
              </div>
              </div>
            </div>
            </div>

@endsection