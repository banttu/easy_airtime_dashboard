<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AirMobi | Admin</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Custom styles for this template -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  
  <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->

</head>

<body>

  <div  class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div  id="sidebar-wrapper">
      <div style=" background-color:#0d4c82 !important;color:white" class="sidebar-heading">AirMobi</div>
      <div style="background-color: #1c1b1b !important" class="list-group list-group-flush">
      <a href="/dashboard" style="background-color: #1c1b1b !important; color:#fff" class="list-group-item list-group-item-action">Users</a>
        <a href="/clients" style="background-color: #1c1b1b !important; color:#fff" class="list-group-item list-group-item-action">Clients</a>
        <!-- <a href="/contacts" style="background-color: #1c1b1b !important; color:#fff" class="list-group-item list-group-item-action">Contacts</a> -->
        <!-- <a href="/mapping" style="background-color: #1c1b1b !important; color:#fff" class="list-group-item list-group-item-action">Contact Mapping</a> -->
        <!-- <a href="/schedules" style="background-color: #1c1b1b !important; color:#fff" class="list-group-item list-group-item-action">Schedules</a> -->
        <a href="/transactions" style="background-color: #1c1b1b !important; color:#fff" class="list-group-item list-group-item-action">Transactions</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav style=" background-color:#0d4c82 !important;color:white" class="navbar navbar-expand-lg navbar-light    border-bottom">
        <button style="height:30px;width:30px;background-color: #FFF" class="navbar-toggler-icon" id="menu-toggle"></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item dropdown">
              <a style="color:#FFF;font-weight:400" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{ Auth::user()->name }}
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                        <i class="icon icon-share-alt"></i>{{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      @yield('content')
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('vendor/jquery/jquery.min.js')"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js')"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

<script src="vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>

</body>

</html>
