@extends('admin.app')

@section('content')

@if ($message=Session::get('error'))
      <div class="alert alert-danger">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Add Contact
          </div>
       <form action="/add-contact" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px;" class="form-group">
           <label style="font-weight:bold;" for="exampleInputEmail1">Phone No</label>
           <div class="input-group">
           <input  id="msisdn" type="msisdn" class="form-control @error('name') is-invalid @enderror" name="msisdn" value="{{ old('name') }}" placeholder="07XXXXXXXX" required autocomplete="msisdn" autofocus>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter contact name"> -->
           </div>

           <label style="font-weight:bold;" for="exampleInputEmail1">Service Provider</label>
           <div class="input-group">
               <select name="telco" class="form-control">
                   <option  value="SAFARICOM">SAFARICOM</option>
                   <option  value="AIRTEL">AIRTEL</option>
               </select> 
               </div>
        <div>
     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection