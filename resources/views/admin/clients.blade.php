@extends('admin.app')

@section('content')

@if ($message=Session::get('success'))
<div class="alert alert-success">
    <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
    <strong>{{$message}}</strong>
  </div>
@endif

      <div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
            Available  Clients
              <!-- <a href="/client-add" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Add client</a> -->
          </div>
          <table class="table table-bordered">
      <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">client Name</th>
      <th  scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php $index = 0;?>
  @foreach ($clients as $client)
    <tr>
      <th scope="row">{{ $index = $index + 1}} .</th>
      <td>{{$client->name}}</td>
      <td>
          <a href="/viewclient/{{$client->id}}"  style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-eye">View</a>
          <!-- <a href="/deleteclient/{{$client->id}}" style="height:20px;background-color:#d91414;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-trash-o">Delete</a> -->
      </td>
    </tr>
          @endforeach
          </tbody>
        </table>
              </div>
              </div>
            </div>
            </div>

@endsection