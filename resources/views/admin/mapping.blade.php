@extends('admin.app')

@section('content')

      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

    <div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;padding:5px;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
                Contact Mappings
              <a href="/mapping-add" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Create mapping</a>
          </div>
         <div table-responsive>
          <table class="table table-bordered">
          <thead>
              <tr>
                <th class="blue-txt" scope="col">No.</th>
                <th class="blue-txt" scope="col">Group Name</th>
                <th class="blue-txt" scope="col">Phone No</th>
                <th class="blue-txt" scope="col">Amount</th>
                <th class="blue-txt"  scope="col">Edit</th>
              </tr>
            </thead>
                <tbody>
                <?php $index = 0;?>
                @foreach ($mappings as $mapping)
                  <tr>
                    <th scope="row" height="5">{{$index = $index+1}}.</th>
                    <td>{{$mapping->group->group_name}}</td>
                    <td>{{$mapping->contact->msisdn}}</td>
                    <td>{{$mapping->amount}}</td>
                    <td style="text-align: center !mportant;">
                        <a href="/map-edit/{{$mapping->id}}" style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-edit">Edit</a>
                    </td>
                    <!-- <td>
                      <a href="/role-delete/{{$mapping->id}}" style="height:20px;background-color:#d91414;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-trash-o" >Delete</a>
                    </td> -->
                  </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      </div>
    </div>
    </div>

@endsection