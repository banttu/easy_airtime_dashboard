@extends('admin.app')

@section('content')

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Add Schedule
          </div>
       <form action="/add-contact" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px;" class="form-group">
           <label style="font-weight:bold;" for="exampleInputEmail1">Group Name</label>
           <div class="input-group">
           <input  id="msisdn" type="msisdn" class="form-control @error('name') is-invalid @enderror" name="msisdn" value="{{$group->group_name}}"  required autocomplete="msisdn" autofocus readonly>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter contact name"> -->
           </div>

           <label style="font-weight:bold;" for="exampleInputEmail1">Client Name</label>
           <div class="input-group">
           <input  id="msisdn" type="msisdn" class="form-control @error('name') is-invalid @enderror" name="msisdn" value="{{$group->clients->name}}"  required autocomplete="msisdn" autofocus readonly>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter contact name"> -->
           </div>


           <label style="font-weight:bold;" for="exampleInputEmail1">Interval Value</label>
           <div class="input-group">
           <input  id="interval_value"   type="datetime-local" class="form-control @error('interval_value') is-invalid @enderror" name="interval_value"  required autocomplete="interval_value	" autofocus >
           </div>

           <label style="font-weight:bold;" for="exampleInputEmail1">Interval Field</label>
           <div class="input-group">
           <input  id="interval_field" class="form-control @error('interval_field') is-invalid @enderror" name="interval_field"  required autocomplete="interval_field	" autofocus >
           </div>

           

        <div>
     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection