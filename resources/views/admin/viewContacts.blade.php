@extends('admin.app')

@section('content')

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      @if ($message=Session::get('success'))
      <div class="alert alert-success">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif

      @if ($message=Session::get('error'))
      <div class="alert alert-danger">
        <button class="close" data-dismiss="alert" aria-label="close">&times;</button>
        <strong>{{$message}}</strong>
      </div>
      @endif
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Add Contact
          </div>
       <form action="/add-contact" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px;" class="form-group">
          <div class="form-row">
              <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1">Phone No</label>
                 <div class="input-group">
                     <input  id="msisdn" type="msisdn" class="form-control @error('name') is-invalid @enderror" name="msisdn" value="{{ old('name') }}" placeholder="07XXXXXXXX" required autocomplete="msisdn" autofocus>
                 </div>
              </div>
              <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1">Service Provider</label>
                <div class="input-group">
                  <select name="telco" class="form-control">
                   <option  value="SAFARICOM">SAFARICOM</option>
                   <option  value="AIRTEL">AIRTEL</option>
                 </select> 
               </div>
              </div>
            </div>

            <div class="form-row">
             <input type="hidden" name="group_id" value='{{$group->id}}'>
              <div class="col">
                <label style="font-weight:bold;" for="exampleInputEmail1">Amount</label>
                 <div class="input-group">
                     <input  id="amount" type="amount" class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}" placeholder="" required autocomplete="amount" autofocus>
                 </div>
              </div>
            </div>
              </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>

      <div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px;border-color:#FFF"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold;justify-content:space-between;width:100%;align-self:center" class="row card-header">
            Available contacts 
              <!-- <a href="/contact-add" style="background-color:#0d4c82;border-color:#FFF" class="btn btn-primary">Add contact</a> -->
          </div>
          <table class="table table-bordered">
      <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Phone No</th>
      <th scope="col">Service Provider</th>
      <th scope="col">Status</th>
      <th scope="col">Amount</th>
      <th  scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php $index = 0;?>
  @foreach ($contacts as $contact)
    <tr>
    <th scope="row">{{ $index = $index + 1}} .</th>
      <td>{{$contact->contacts[0]->msisdn}}</td>
      <td>{{$contact->contacts[0]->telco}}</td>
      <td>{{$contact->contacts[0]->status}}</td>
      <td>{{$contact->amount}}</td>
      <td>
          <a href="/editcontact/{{$contact->contacts[0]->id}}"  style="background-color:#0d4c82;border-radius: 5px;padding:3px;color:#FFF;" class="fa fa-edit">Edit</a>
          <!-- <a href="/deletecontact/{{$contact->id}}" style="height:20px;background-color:#d91414;border-radius: 5px;padding:3px;color:#FFF" class="fa fa-trash-o">Delete</a> -->
      </td>
    </tr>
          @endforeach
          </tbody>
        </table>
              </div>
              </div>
            </div>
            </div>

@endsection