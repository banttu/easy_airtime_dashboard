@extends('admin.app')

@section('content')

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Add Client
          </div>
       <form action="/add-client" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-bottom:10px;margin-right:10px ">
           <label style="font-weight:bold;" for="exampleInputEmail1">Client Name</label>
           <div class="input-group">
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter client name"> -->
           </div>
        <div>
     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection