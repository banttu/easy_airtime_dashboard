@extends('admin.app')

@section('content')

     <div class="row justify-content-center" style="margin-top:20px;padding:10px">

      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Edit Client
          </div>
          <form action="/update-client/{{$client->id}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
        <div class="md-form mb-6">

        <div class="form-group">
                <label for="name" style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px;font-weight:bold">Client</label>
                
               <div class="input-group">
               <input style="margin-left: 20px;margin-top: 5px;margin-bottom: 5px;color:black;margin-right:20px" id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value=" {{$client->name}}" required autocomplete="name" autofocus>
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter client name"> -->
               </div>
        </div>
            
            </div>
            <div class="row" style="margin-left:4px">
                    <button style="margin-top: 29px;background-color:#0d4c82;margin-bottom:29px;margin-left:20px" type="submit" class="btn btn-success">
                            Submit
                    </button> 

                    <a href="/clients" style="margin-top: 29px;margin-bottom:29px;margin-left:20px" class="btn btn-danger">
                            Cancel
                    </a> 
            </div>
         </div>
     </form>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection