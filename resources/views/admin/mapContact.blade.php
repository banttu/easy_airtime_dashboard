@extends('admin.app')

@section('content')

<div class="row justify-content-center" style="margin-top:20px;padding:10px">
      <div class="col-md-10">
      <div style="margin-top:20px"  class="card">
          <div style="background-color:#0d4c82;color:white;font-weight:bold" class="card-header">
              Map Contact
          </div>
       <form action="/contactMap" method="POST">
                        {{csrf_field()}}
                        {{method_field('POST')}}
       <div style="margin-left:10px;margin-right:10px" class="form-group">
           <label style="font-weight:bold;" for="exampleInputEmail1">Phone Number</label>
           <div class="input-group">
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="msisdn" type="msisdn" class="form-control @error('msisdn') is-invalid @enderror" name="msisdn" value=" {{$contact->msisdn}}" required autocomplete="msisdn" autofocus>
           
           <input name="contact_id" type="hidden" value=" {{$contact->id}}" /> 
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter client name"> -->
           </div>
        <div>

        <div class="form-group">
                <label for="group_id" style="font-weight:bold">Select Group</label>
            <div class="input-group">
               <select name="group_id" class="form-control">
                   @foreach ($groups as $group)
                   <option  value="{{$group->id}}">{{$group->client->name}} , {{$group->group_name}}</option>
                   @endforeach
               </select> 
               </div>
        </div>

        <div class="form-group">
           <label style="font-weight:bold;" for="exampleInputEmail1">Amount</label>
           <div class="input-group">
           <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="amount"  class="form-control @error('name') is-invalid @enderror" name="amount" value="{{ old('amount') }}" required autocomplete="amount" autofocus>
           <!-- <input style="margin-left: 4px;width: 450px;margin-top: 5px;margin-bottom: 5px;" id="amount" type="amount" class="form-control @error('amount') is-invalid @enderror" name="amount" required autocomplete="amount" autofocus> -->
              <!-- <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter client name"> -->
           </div>
        <div>

     </form>
             <button  type="submit" class="btn btn-primary" style="margin-top: 29px;background-color:#0d4c82">
                 Submit
             </button>
          </a>
    </div>

      </div>
      </div>
    </div>
    </div>

@endsection