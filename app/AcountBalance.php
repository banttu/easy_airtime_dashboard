<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcountBalance extends Model
{
    protected $fillable = ['client_id','balance' ];
}
