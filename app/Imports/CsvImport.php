<?php

namespace App\Imports;

use App\Contacts;
use App\ContactMapping;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CsvImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // return new Contacts([
        //     'msisdn'      =>  $row["msisdn"],
        //     'telco'     =>  $row["telco"],
        // ]);

        

        $contact = new Contacts();
        $contact->msisdn =  $row["msisdn"];
        $contact->telco =$row["telco"];
        $contact->save();

        $mapping = new ContactMapping();
        $mapping->contact_id = $contact->id;
        $mapping->client_id = Auth::user()->id;
        $mapping->group_id = request('group_id');
        $mapping->amount = $row["amount"];
        $mapping->save();

       
    }
}


