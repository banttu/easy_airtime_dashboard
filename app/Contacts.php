<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContactGroups;

class Contacts extends Model
{
    protected $primaryKey = 'id'; 
    
    protected $table = 'contacts';
       
    protected $fillable = ['status','msisdn','telco'];


    public function mappings(){
        return $this->hasMany(ContactMapping::class,'id', 'contact_id');
    }
    

}
