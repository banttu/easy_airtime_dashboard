<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactGroups extends Model
{
    protected $primaryKey = 'id'; 
    
    protected $table = 'contact_groups';
       
    protected $fillable = ['client_id','group_name','status'];

    public function client(){
        return $this->belongsTo(Clients::class, 'client_id');
   }

}
