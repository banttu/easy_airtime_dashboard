<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
	
	protected $primaryKey = 'id'; 
    
    protected $table = 'groups';
       
    protected $fillable = ['account_id', 'group_name'];

    public function topupnumbers(){
            return $this->hasMany('App\Topup_number', 'id', 'group_id');
    }

}