<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\ContactGroups;
use App\Clients;
use App\Contacts;
use App\ContactMapping;
use App\Schedules;
use App\AirTimeTransactions;
use Auth;
use Redirect;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = Clients::where('user_id',Auth::user()->id)->first();
        $groups = ContactGroups::where('client_id',Auth::user()->id)->get();
        $users = User::where('id', '!=', auth()->id())->get();
        return view('admin.index',compact('users','client','groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerdelete($id)
    {
        $users = User::findOrFail($id);
        $users->delete();
        return redirect('/dashboard')->with('success','User Deleted Successfully!!');
    }

    public function create_user(){

        return view ('admin.createUser');
     

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function groups()
    {
       $groups = ContactGroups::with('client')->get();
      return($groups);
      return view('admin.groups', compact('groups'));
    }

    public function store_user(Request $request){

        $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string','max:10','min:10', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'company_name'=>['required','string'],
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password =  Hash::make($request->password);
        $user->save();

        $client = new Clients();
        $client->user_id = $user->id;
        $client->name = $request['company_name'];
        $client->save();

        return redirect ('/dashboard')->with('success','user added successfully');


        // 'name' => $data['name'],
        // 'email' => $data['email'],
        // 'password' => Hash::make($data['password']),

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addgroup()
    {
     $clients = Clients::all();
      return view ('admin.addGroup',compact('clients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function groupadd (Request $request){
        
        $this->validate($request,[
            'group_name'=>'required|min:2|max:50',
        ]);
       $group = New ContactGroups();
       $group->group_name = $request->group_name;
       $group->client_id=$request->client_id;
       $group->save();
    //    return ['message'=>'OK'];
       return redirect('/groups')->with('success','Group added successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientadd()
    {
        return view('admin.addClient');
        
    }

    public function addclient(Request $request){
        $this->validate($request,[
            'name'=>'required|min:2|max:50'
        ]);
       $client = New Clients();
       $client->name = $request->name;
       $client->save();
    //    return ['message'=>'OK'];
       return redirect('/clients')->with('success','Client added successfully');
    }

    public function viewclient($id){

        $groups = ContactGroups::with('client')->where('client_id',$id)->orderBy('id','desc')->get();

        // $client = Clients::find($id);
        // return ($groups);
        return view('admin.viewClient',compact('groups'));
    }

    public function updateclient (Request $request, $id){

        $this->validate($request,[
            'name'=>'required|min:2|max:50'
        ]);
        $client = Clients::find($id);
        $client->name = $request->name;
        $client->save();

        return redirect('/clients')->with('success','Client Updated Successfully!!');
    }

    public function editGroup($id){
        $group = ContactGroups::find($id);
      return view('admin.editGroup',compact('group'));
    }

    public function updateGroup(Request $request, $id){
        
        $this->validate($request,[
            'group_name'=>'required|min:2|max:50'
        ]);
        $group = ContactGroups::find($id);
        $group->group_name = $request->group_name;
        $group->status = $request->status;
        $group->save();

        return redirect('/groups')->with('success','Group Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clients()
    {
        $clients = Clients::all();
        return view ('admin.clients',compact('clients'));
    }

    public function viewcontacts($id){
        $contact = ContactMapping::with('contacts')->where('group_id',$id)->get();
        $group = ContactGroups::find($id);
        // return($contact[0]);
        // $contacts = $contact[0];
        if($contact->isEmpty()){
         $contacts = $contact;
        }else{
         $contacts = $contact;
        }
        // return($contacts);
        // return($singleContacts);
        return view ('admin.viewContacts',compact('contacts','group'));
       
    }

    public function contacts()
    {
        $contacts = Contacts::all();
        // return($contacts);
        return view ('admin.contacts',compact('contacts'));
    }

    public function addcontact(){
        return view('admin.addContact');
    }

    public function createcontact(Request $request){
        $this->validate($request,[
            'msisdn' => ['required', 'string', 'max:10','min:10','unique:users'],
            'amount' => ['required'],
        ]);

        if (Contacts::where('msisdn', '=', $request->msisdn)->exists()) {
           return Redirect::back()->with('error', 'Contact Already Exists');

         }else{

         
        $contact = new Contacts();
        $contact->msisdn = $request->msisdn;
        $contact->telco = $request->telco;
        $contact->save();

        $mapping = new ContactMapping();
        $mapping->contact_id = $contact->id;
        $mapping->client_id = Auth::user()->id;
        $mapping->group_id = $request->group_id;
        $mapping->amount = $request->amount;
        $mapping->save();

        return redirect ('/clients')->with('success','Contact added successfully');
         }
    }

    public function editcontact($id){
        $contact = Contacts::find($id);
        return view('admin.editContact',compact('contact'));
    }

    public function updateContact(Request $request, $id){
    
        $contact = Contacts::find($id);
        $contact->status = $request->status;
        $contact->save();

        return redirect('/contacts')->with('success','Contact Updated Successfully!!');
    }
    public function mapping(){
    $mappings = ContactMapping::all();
    // $contact  = Contacts::find(1);
    //  return response()->json([
    //     'mappings'=>$mappings,
    // ],200);
    // return($mappings);
     return view('admin.mapping', compact('mappings'));
    }
    public function addmapping(){
     $groups = ContactGroups::all();
     $contacts = Contacts::all();
    //  return ($contacts);
     return view('admin.addMapping',compact('groups','contacts'));
    }
    public function mappcontact($id){
        $contact = Contacts::find($id);
        $groups = ContactGroups::with('client')->get();
        return($groups);
        return view('admin.mapContact',compact('contact','groups'));
    }

    public function contactMap (Request $request){

        $this->validate($request,[
            'amount'=>'required',
        ]);
        $map = new ContactMapping();
        $map->contact_id = $request->contact_id;
        $map->group_id = $request->group_id;
        $map->amount = $request->amount;
        $map->save();

        return redirect('/mapping')->with('success','Contact Mapped Successfully!!');
    }

    public function mapedit($id){

        $mapping = ContactMapping::with('contact','group')->where('id',$id)->get();

        $groups = ContactGroups::all();

        // return($mapping);

        return view('admin.editMapping',compact('mapping','groups'));

    }

    public function editmap (Request $request,$id){
        
        $map = ContactMapping::find($id);
        $map->contact_id = $request->contact_id;
        $map->group_id = $request->group_id;
        $map->amount = $request->amount;
        $map->save();

        return redirect('/mapping')->with('success','Mapping Updated Successfully!!');

    }

    public function saveschedule(Request $request, $id){
        $schedule = new Schedules();
        $schedule->group_id = $id;
        $schedule->interval_value = $request->interval_value;
        $schedule->interval_field = $request->interval_field;
        $schedule->save();
        return redirect('/clients')->with('success','Schedule added Successfully!!');
       }

       public function editschedule($id){
        $schedule = Schedules::find($id);
       //  return($schedule);
        return view('admin.editSchedule',compact('schedule'));
       }
       
       public function updateschedule(Request $request,$id){
        $requestData = $request->all();
        $schedule = Schedules::find($id);
        $schedule->update($requestData);
        return redirect('/clients')->with('success','Schedule Updated Successfully!!');
       }
   
   

    public function schedules($id){
        $group = ContactGroups::find($id);
        $schedules = Schedules::with('groups')->where('group_id',$id)->get();
        // return($schedules);
        return view('admin.schedules',compact('schedules','group'));
    }

    public function addschedule(){
        $groups = ContactGroups::with('client')->get();
        // return($groups);
        return view('admin.addSchedule',compact('groups'));
    }

    public function schedulecreate($id){
        $group = ContactGroups::with('clients')->where('id',$id)->first();
        //return($group);
        return view('admin.createSchedule',compact('group'));
    }

    public function transactions(){
      $transactions = AirTimeTransactions::all();
     return view('admin.transactions',compact('transactions'));
    }

}
