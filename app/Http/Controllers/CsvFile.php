<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\CsvImport;
use Redirect;

class CsvFile extends Controller
{
   public function csv_import(){
       \Excel::import(new CsvImport,request()->file('file'));
       return Redirect::back()->with('success','Contacts Uploaded successfully');
   }
}
