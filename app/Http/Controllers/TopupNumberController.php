<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Topup_number;

use Auth;
use Session;

class TopupNumberController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        $perPage = 10; 
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $topup_numbers = Topup_number::where('account_id', 
                $account_id)->orderBy('id', 'DESC')->paginate($perPage); 
                
        return view('user.topup_numbers', compact('topup_numbers'));        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $requestData["account_id"] = $account_id;
        
        Topup_number::create($requestData);    

        Session::flash('flash_message', 'Topup Number Record added!');

        return redirect('numbers');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $perPage = 5;
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $topup_numbers = Topup_number::where('account_id', $account_id)
                ->orderBy('id', 'DESC')->paginate($perPage);
        
        $topup_number = Topup_number::findOrFail($id);
        $submitButtonText = 'Update';
        
        return view('user.topup_numbers', compact('topup_number', 'submitButtonText',
                'topup_numbers'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $topup_number = Topup_number::findOrFail($id);
        $topup_number->update($requestData);

        Session::flash('flash_message', 'Topup Number record updated!');
        
        return redirect('numbers');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {       
        $id = $request->group_id;
        Topup_number::destroy($id);

        Session::flash('flash_message', 'Topup number record deleted!');

        return redirect('numbers');
    }
    
}