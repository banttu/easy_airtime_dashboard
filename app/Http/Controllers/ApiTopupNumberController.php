<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Topup_number;

use Auth;
use Session;

class ApiTopupNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10; 
        
        $account_id = Auth::user()->id;
        
        $topup_numbers = Topup_number::where('account_id', 
                $account_id)->orderBy('id', 'DESC')->paginate($perPage); 

      return ($topup_numbers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        
        $account_id = Auth::user()->id;

        $requestData["account_id"] = $account_id;
        
        Topup_number::create($requestData);    

        $out = [
            'message'   => 'Topup Number Record added!'
        ];
        return response()->json($out,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        
        $topup_number = Topup_number::findOrFail($id);
        $topup_number->update($requestData);

        $out = [
            'message'   => 'Topup Number record updated!'
        ];
        return response()->json($out);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        Topup_number::destroy($id);

        $out = [
            'message'   => 'Topup number record deleted!'
        ];
        return response()->json($out);
    }
}
