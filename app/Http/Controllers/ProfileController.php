<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;

use Session;
use Auth;

class ProfileController extends Controller
{
    
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

      $profile = User::findorfail(Auth::user()->id);
      return view('user.profile', compact('profile'));
    }
    

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
//        $userProfile = UserProfile::findOrFail($id);
//       
//        return view('client::show', compact('userProfile'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $userProfile = User::findOrFail($id);
        
        return view('edit', compact('userProfile'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();

        //dd( $request->all());
        
        $userProfile = User::findOrFail($id);
        $userProfile->update($requestData);

        Session::flash('flash_message', 'Profile updated!');
        
        return redirect('profile');
    }
    

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        
    }
}