<?php

namespace App\Http\Controllers;

use App\ContactMapping;
use Illuminate\Http\Request;

class ContactMappingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactMapping  $contactMapping
     * @return \Illuminate\Http\Response
     */
    public function show(ContactMapping $contactMapping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactMapping  $contactMapping
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactMapping $contactMapping)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactMapping  $contactMapping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactMapping $contactMapping)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactMapping  $contactMapping
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactMapping $contactMapping)
    {
        //
    }
}
