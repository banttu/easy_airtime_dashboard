<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Schedule;

use Auth;
use Session;

class ScheduleController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        $perPage = 20; 
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $schedules = Schedule::where('account_id', 
                $account_id)->orderBy('id', 'DESC')->paginate($perPage); 
                
        return view('user.schedules', compact('schedules'));        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $requestData["account_id"] = $account_id;
        
        Schedule::create($requestData);    

        Session::flash('flash_message', 'Schedule record added!');

        return redirect('schedules');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $perPage = 20;
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $schedules = Schedule::where('account_id', $account_id)
                ->orderBy('id', 'DESC')->paginate($perPage);
        
        $schedule = Schedule::findOrFail($id);
        $submitButtonText = 'Update';
        
        return view('user.schedules', compact('schedule', 'submitButtonText',
                'schedules'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $schedule = Schedule::findOrFail($id);
        $schedule->update($requestData);

        Session::flash('flash_message', 'Schedule record updated!');
        
        return redirect('schedules');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {       
        $id = $request->schedule_id;
        Schedule::destroy($id);

        Session::flash('flash_message', 'Schedule record deleted!');

        return redirect('schedules');
    }
    
}