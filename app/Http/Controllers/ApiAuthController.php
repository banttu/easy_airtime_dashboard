<?php

namespace App\Http\Controllers;

use App\User;
use App\Clients;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ApiAuthController extends Controller
{
    public function register(Request $request){
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'company_name' => ['required', 'string', 'max:255'],
            'phone' => 'required|string|max:10|min:10',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);

        $user = new User();
        $user->name = $data['name'];
        $user->phone = $data['phone'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();

        $client = new Clients();
        $client->user_id = $user->id;
        $client->name = $data['company_name'];
        $client->save();
        
        $out = [
            'success'   => true,
            'user_id'   => $user->id,
            'name'   => $user->name,
            'message'   => 'Registration successful'
        ];
        return response()->json($out,201);
        }
        
      public function login(Request $request){
                $email= $request->email;
                $password = $request->password;
        
                $users = User::where('email', $email)->get();
        
                if(count($users)>0){
                    $user = User::where([['email', $email]])->first();
                    if(password_verify($password, $user->password)) {
                        $tokenResult = $user->createToken('Personal Access Token');
                        $token = $tokenResult->token;
                        if ($request->remember_me) {
                            $token->expires_at = Carbon::now()->addWeeks(1);
                        }
                        $token->save();
                
                        return response()->json([
                            'success'      => true,
                            'access_token' => $tokenResult->accessToken,
                            'token_type'   => 'Bearer',
                            'expires_at'   => Carbon::parse(
                                $tokenResult->token->expires_at
                            )->toDateTimeString(),
                            'user'         => $user,
                        ]);
                
                    }else{
                        $response = array(
                            "success" => "false",
                            "message" => "Incorrect Password" 
                        );
            
                        return response()->json($response,401);
                    }
                }else{
                    $response = array(
                        "success" => "false",
                        "message" => "User not found" 
                    );
        
                    return response()->json($response,401);
                }
            }
}
