<?php

namespace App\Http\Controllers;

use App\ContactGroups;
use Illuminate\Http\Request;

class ContactGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactGroups  $contactGroups
     * @return \Illuminate\Http\Response
     */
    public function show(ContactGroups $contactGroups)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactGroups  $contactGroups
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactGroups $contactGroups)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactGroups  $contactGroups
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactGroups $contactGroups)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactGroups  $contactGroups
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactGroups $contactGroups)
    {
        //
    }
}
