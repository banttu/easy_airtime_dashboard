<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\ContactGroups;
use App\Clients;
use App\Contacts;
use App\ContactMapping;
use App\Schedules;
use App\AirTimeTransactions;
use Illuminate\Support\Facades\Auth;
use DB;
use Redirect;
use App\AcountBalance;


class ClientController extends Controller
{
  
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $client = Clients::where('user_id',Auth::user()->id)->first();

        // return($client->id);

        $mappings = ContactMapping::with('group','contacts')->where('client_id',Auth::user()->id)->get();
           
        $groups = ContactGroups::where('client_id',Auth::user()->id)->get();

        $groupCount = ContactGroups::where('client_id',Auth::user()->id)->count();

        $contactCount = Contacts::count();
        $balance = AcountBalance::where('client_id',Auth::user()->id)->first();
        //  return($mappings);

        return view('client.index',compact('mappings','groups','client','contactCount','groupCount','balance'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerdelete($id)
    {
        $users = User::findOrFail($id);
        $users->delete();
        return redirect('/dashboard')->with('success','User Deleted Successfully!!');
    }

    public function create_user(){

        return view ('client.createUser');
     

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function groups()
    {
       $client = Clients::where('user_id',Auth::user()->id)->first();
      
    //    return($client);
       
       $groups = ContactGroups::where('client_id',$client->id)->get();
    //   return($groups);
      return view('client.groups', compact('groups','client'));
    }

    public function store_user(Request $request){

        $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string','max:10','min:10', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'company_name'=>['required','string'],
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password =  Hash::make($request->password);
        $user->save();

        $client = new Clients();
        $client->user_id = $user->id;
        $client->name = $request['company_name'];
        $client->save();

        return redirect ('/dashboard')->with('success','user added successfully');


        // 'name' => $data['name'],
        // 'email' => $data['email'],
        // 'password' => Hash::make($data['password']),

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addgroup()
    {
     $clients = Clients::where('user_id',Auth::user()->id)->first();
      return view ('client.addGroup',compact('clients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function groupadd (Request $request){
        
        $this->validate($request,[
            'group_name'=>'required|min:2|max:50',
        ]);

       $client = Clients::where('user_id',Auth::user()->id)->first();
        
       $group = new ContactGroups();
       $group->group_name = $request->group_name;
       $group->client_id=$client->id;
       $group->save();
    //    return ['message'=>'OK'];
       return redirect('/client-groups')->with('success','Group added successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientadd()
    {
        return view('client.addClient');
        
    }

    public function addclient(Request $request){
        $this->validate($request,[
            'name'=>'required|min:2|max:50'
        ]);
       $client = New Clients();
       $client->name = $request->name;
       $client->save();
    //    return ['message'=>'OK'];
       return redirect('/clients')->with('success','Client added successfully');
    }

    public function viewclient($id){

        $groups = ContactGroups::with('client')->where('client_id',$id)->orderBy('id','desc')->get();

        // $client = Clients::find($id);
        // return ($groups);
        return view('client.viewClient',compact('groups'));
    }

    public function updateclient (Request $request, $id){

        $this->validate($request,[
            'name'=>'required|min:2|max:50'
        ]);
        $client = Clients::find($id);
        $client->name = $request->name;
        $client->save();

        return redirect('/clients')->with('success','Client Updated Successfully!!');
    }

    public function editGroup($id){
        $client = Clients::where('user_id',Auth::user()->id)->first();
        $group = ContactGroups::find($id);
      return view('client.editGroup',compact('group','client'));
    }

    public function updateGroup(Request $request, $id){
        
        $this->validate($request,[
            'group_name'=>'required|min:2|max:50'
        ]);
        $group = ContactGroups::find($id);
        $group->group_name = $request->group_name;
        $group->status = $request->status;
        $group->save();

        return redirect('/client-groups')->with('success','Group Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clients()
    {
        $clients = Clients::all();
        return view ('client.clients',compact('clients'));
    }

    public function viewcontacts($id){
        $client = Clients::where('user_id',Auth::user()->id)->first();
        $group = ContactGroups::find($id);
        $contacts = ContactMapping::with('contacts')->where('group_id',$id)->get();
        // return($group);
        return view ('client.viewContacts',compact('contacts','group','client'));
    }

    public function contacts()
    {
        $contacts = Contacts::all();
        // return($contacts);
        return view ('client.contacts',compact('contacts'));
    }

    public function addcontact($id){
        $client = Clients::where('user_id',Auth::user()->id)->first();
        $group = ContactGroups::find($id);
        // return($group);
        return view('client.addContact',compact('group','client'));
    }

    public function createcontact(Request $request){
        $this->validate($request,[
            'msisdn' => ['required', 'string', 'max:10','min:10'],
            'amount' => ['required'],
        ]);

        if (Contacts::where('msisdn', '=', $request->msisdn)->exists()) {
           return Redirect::back()->with('error', 'Contact Already Exists');

         }else{

         
        $contact = new Contacts();
        $contact->msisdn = $request->msisdn;
        $contact->telco = $request->telco;
        $contact->save();

        $mapping = new ContactMapping();
        $mapping->contact_id = $contact->id;
        $mapping->client_id = Auth::user()->id;
        $mapping->group_id = $request->group_id;
        $mapping->amount = $request->amount;
        $mapping->save();

        return redirect ('addContact/'.$request->group_id)->with('success','Contact added successfully');
        // return redirect('student_results/'.$request->student_id);
         }
    }
    public function editcontact($id){
        $client = Clients::where('user_id',Auth::user()->id)->first();
        $contact = Contacts::find($id);
        return view('client.editContact',compact('contact','client'));
    }

    public function updateContact(Request $request, $id){
    
        $contact = Contacts::find($id);
        $contact->status = $request->status;
        $contact->save();

        return redirect('client-group/'.$request->group_id)->with('success','Contact Updated Successfully!!');
    }
    public function mapping(){
    $mappings = ContactMapping::all();
    // $contact  = Contacts::find(1);
     return response()->json([
        'mappings'=>$mappings,
    ],200);
     return view('client.mapping', compact('mappings'));
    }
    public function addmapping(){
     $client = Clients::where('user_id',Auth::user()->id)->first();
     $groups = ContactGroups::where('client_id',Auth::user()->id);
    //  return response()->json([
    //     'groups'=>$groups,
    // ],200);
     return view('client.addContact',compact('groups','client'));
    }
    public function mappcontact($id){
        $contact = Contacts::find($id);
        $groups = ContactGroups::with('client')->get();
        // return($groups);
        return view('client.mapContact',compact('contact','groups'));
    }

    public function contactMap (Request $request){

        $this->validate($request,[
            'amount'=>'required',
        ]);
        $map = new ContactMapping();
        $map->contact_id = $request->contact_id;
        $map->group_id = $request->group_id;
        $map->amount = $request->amount;
        $map->save();

        return redirect('/client')->with('success','Contact Mapped Successfully!!');
    }

    public function mapedit($id){

        $mapping = ContactMapping::with('contact','group')->where('id',$id)->get();
        $client = Clients::where('user_id',Auth::user()->id)->first();
        // return($mapping);

        $groups = ContactGroups::all();

        // return($groups);

        return view('client.editMapping',compact('mapping','groups','client'));

    }

    public function editmap (Request $request,$id){



        $contact_id = $request->contact_id;
        
        $map = ContactMapping::find($id);
        $map->contact_id = $request->contact_id;
        $map->group_id = $request->group_id;
        $map->amount = $request->amount;
        $map->save();

        $contact = Contacts::find($contact_id);
        $contact->msisdn = $request->msisdn;
        $contact->telco = $request->telco;
        $contact->save();

        return redirect('/client')->with('success','Mapping Updated Successfully!!');

    }

    public function schedules($id){
        $client = Clients::where('user_id',Auth::user()->id)->first();
        $group = ContactGroups::find($id);
        $schedules = Schedules::with('groups')->where('group_id',$id)->get();
        $contacts = contactMapping::with('contacts')->where('group_id',$id)->get();
        // return($contacts);
        return view('client.schedules',compact('schedules','group','contacts','client'));
    }

    public function addschedule($id){
        $client = Clients::where('user_id',Auth::user()->id)->first();
        $group = ContactGroups::find($id);
        // return($groups);
        return view('client.addSchedule',compact('group','client'));
    }

    public function schedulecreate($id){
        $group = ContactGroups::with('clients')->where('id',$id)->first();
        $client = Clients::where('user_id',Auth::user()->id)->first();
        //return($group);
        return view('client.createSchedule',compact('group','client'));
    }

    public function saveschedule(Request $request, $id){
     $schedule = new Schedules();
     $schedule->group_id = $id;
     $schedule->interval_value = $request->interval_value;
     $schedule->interval_field = $request->interval_field;
     $schedule->save();
     return redirect('/client-groups')->with('success','Schedule added Successfully!!');
    }

    public function editschedules($id){
     $schedule = Schedules::with('groups')->where('id',$id)->first();
    //  return($schedule);
     return view('client.editSchedule',compact('schedule'));
    }

    public function updateschedule(Request $request,$id){
     $requestData = $request->all();
     $schedule = Schedules::find($id);
     $schedule->update($requestData);
     return redirect('/client-groups')->with('success','Schedule Updated Successfully!!');
    }

    public function transactions(){
    //   $transactions = AirTimeTransactions::all();
      $transactions = DB::table('airtime_transactions')
            ->join('contact_mapping', 'airtime_transactions.map_id', '=', 'contact_mapping.id')
            ->join('contact_groups', 'contact_mapping.group_id', '=', 'contact_groups.id')
            ->join('contacts', function($join){ 
                $join->on('contact_mapping.contact_id', '=', 'contacts.id');
                    // ->on('contact_mapping.client_id', '=', Auth::user()->id);
            })->where('contact_mapping.client_id', '=', Auth::user()->id)
            ->get();
            // return($transactions);
      $client = Clients::where('user_id',Auth::user()->id)->first();
     return view('client.transactions',compact('transactions','client'));
    }

    public function getDownload($file_name){
   //CSV file is stored under project/public/download/sample.csv
   $file_path = public_path('download/'.$file_name);
   return response()->download($file_path);
}
    
}
