<?php

namespace App\Http\Controllers;

use App\AcountBalance;
use Illuminate\Http\Request;

class AcountBalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AcountBalance  $acountBalance
     * @return \Illuminate\Http\Response
     */
    public function show(AcountBalance $acountBalance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcountBalance  $acountBalance
     * @return \Illuminate\Http\Response
     */
    public function edit(AcountBalance $acountBalance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcountBalance  $acountBalance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AcountBalance $acountBalance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcountBalance  $acountBalance
     * @return \Illuminate\Http\Response
     */
    public function destroy(AcountBalance $acountBalance)
    {
        //
    }
}
