<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Airtime_log;

use Auth;
use Session;

class AirtimeLogController extends Controller
{
	
	
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        $perPage = 50; 
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $airtime_logs = Airtime_log::where('account_id', 
                $account_id)->orderBy('id', 'DESC')->paginate($perPage); 
                
        return view('user.topup_logs', compact('airtime_logs'));        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {       
    }
    
}