<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Topup_number;

use App\Group;

use Auth;
use Session;
class ApiGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_id = Auth::user()->id;
        //$account_id = Auth::user()->account_id;
        $perPage = 50;

        $groups = Group::where('account_id', $account_id)->orderBy('id', 'DESC')->paginate($perPage); 
        return ($groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        
        $account_id = Auth::user()->id;
        
        $requestData["account_id"] = $account_id;
        
        Group::create($requestData);    

        $out = [
            'message'   => 'Group Record created!'
        ];
        return response()->json($out,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Group::findOrfail($id);
        $group_id = $group->id;
        $topup_numbers = Topup_number::where('group_id', 
        $group_id)->orderBy('id', 'DESC')->get(); 

      //return ($topup_numbers);
      return ($topup_numbers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        
        $group = Group::findOrFail($id);
        $group->update($requestData);

        
        $out = [
            'message'   => 'Group Record Updated!'
        ];
        return response()->json($out);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {       
        $id = $request->id;
        Group::destroy($id);

        $out = [
            'message'   => 'Group record deleted!'
        ];
        return response()->json($out);
    }
}
