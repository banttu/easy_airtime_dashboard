<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use Session;
use Auth;
use App\Contacts;
use App\ContactGroups;
use App\ContactMapping;
use App\Schedules;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
      $profile = User::findorfail(Auth::user()->id);
    //   return ($profile);
      return response()->json($profile);
    }

    public function updateProfile($id, Request $request)
    {
        
        $requestData = $request->all();

        //dd( $request->all());
        
        $userProfile = User::findOrFail($id);
        $userProfile->update($requestData);

        $out = [
            'success'   => true,
            'message'   => 'Profile updated!'
        ];
        return response()->json($out);
    }

    public function groups()
    {
        $client_id = Auth::user()->id;
        $groups = ContactGroups::where('client_id',$client_id)->get();
        // return ($groups);
        return response()->json($groups);
    }

    public function creategroup(Request $request){
        $requestData = $request->all();
        
        $client_id = Auth::user()->id;
        
        $requestData["client_id"] = $client_id;
        
        ContactGroups::create($requestData);    

        $out = [
            'success'   => true,
            'message'   => 'Group created!'
        ];
        return response()->json($out,201);

    }

    public function updategroup(Request $request, $id)
    {
        $requestData = $request->all();
        
        $group = ContactGroups::findOrFail($id);
        $group->update($requestData);

        
        $out = [
            'success'   => true,
            'message'   => 'Group Updated!'
        ];
        return response()->json($out);
    }

    public function showgroup($id){
        $schedules = Schedules::with('groups')->where('group_id',$id)->get();
        $contacts = ContactMapping::with('contacts')->where('group_id',$id)->get();

        $response = array(
            "contacts" => $contacts,
            "schedules" => $schedules,
        );

        return response()->json($response,200);

    }

    public function deletegroup($id){
        ContactGroups::destroy($id);

        $out = [
            'success'   => true,
            'message'   => 'Group deleted!'
        ];
        return response()->json($out);
    }

    public function storeContact(Request $request)
    {
        $requestData = $request->all();
        if (Contacts::where('msisdn', '=', $request->msisdn)->exists()) {

            $out = [
                'success'   => false,
                'message'   => 'Contact Already Exists'
            ];
            return response()->json($out,403);
 
          }else{
 
          
         $contact = new Contacts();
         $contact->msisdn = $request->msisdn;
         $contact->telco = $request->telco;
         $contact->save();
 
         $mapping = new ContactMapping();
         $mapping->contact_id = $contact->id;
         $mapping->client_id = Auth::user()->id;
         $mapping->group_id = $request->group_id;
         $mapping->amount = $request->amount;
         $mapping->save();

         $out = [
            'success'   => true,
            'message'   => 'Contact added successfully'
        ];
          return response()->json($out,201);
       }
    }

    public function updateContact(Request $request, $id){
        $contact = Contacts::find($id);
        $contact->status = $request->status;
        $contact->save();

        $out = [
            'success'   => true,
            'message'   => 'Contact updated!'
        ];
        return response()->json($out);
    }

    public function destroyContact($id){

        $contact = Contacts::find($id);
        $contact->status = 'DELETED';
        $contact->save();

        $out = [
            'success'   => true,
            'message'   => 'Contact Status Changed!'
        ];
        return response()->json($out);

    }

    public function storeSchedule(Request $request,$id){
        $schedule = new Schedules();
        $schedule->group_id = $id;
        $schedule->interval_value = $request->interval_value;
        $schedule->interval_field = $request->interval_field;
        $schedule->save();

        $out = [
            'success'   => true,
            'message'   => 'Schedule Saved Successfully!'
        ];
        return response()->json($out);

    }

       public function updateschedule(Request $request,$id){
        $requestData = $request->all();
        $schedule = Schedules::find($id);
        $schedule->update($requestData);

        $out = [
            'success'   => true,
            'message'   => 'Schedule Updated Successfully!'
        ];
        return response()->json($out);

       }

      public function destroySchedule($id){
        $schedule = Schedules::find($id);
        $schedule->delete();

        $out = [
            'success'   => true,
            'message'   => 'Schedule deleted Successfully!'
        ];
        return response()->json($out);
       }
    
}
