<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class ApiUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return ($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->isMethod('put') ? User::findOrFail($request->user_id) : new User;

        $user->first_name = $request->input('first_name');
        $user->email = $request->input('email');;
        $user->password = Hash::make($request->input('password'));

    
        $existingUser = User::where('email',  $request->input('email'))->first();
        if($existingUser)
        {
            $response = array(
                "success" => "false",
                "message" => "User Already exists" 
            );
            return response()->json($response,409);
        }
        $user->save();
        $response = array(
            "status_code" => "001",
            "message" => "Registratered Successfully" 
        );
     return response()->json($response,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return ($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function login(Request $request)
    {

        //Log::info($request);
        $email = $request->email;
        $password = $request->password;

        $users = User::where('email', $email)->get();

        if(count($users)>0){
            $user = User::where([['email', $email]])->first();
            if(password_verify($password, $user->password)) {
                // in case if "$crypt_password_string" actually hides "1234567"
                
                $response = array(
                    "success" => "true",
                    "message" => "User logged in!" 
                );
                return response()->json($response,200);
            }else{
                $response = array(
                    "success" => "false",
                    "message" => "Incorrect Password" 
                );
    
                return response()->json($response,401);
            }
        }else{
            $response = array(
                "success" => "false",
                "message" => "User not found" 
            );

            return response()->json($response,401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
