<?php

namespace App\Http\Controllers;

use App\Group;
use App\Topup_number;
use App\CsvData;
use App\Http\Requests\CsvImportRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class ImportController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function parseImport(CsvImportRequest $request) {

        $path = $request->file('csv_file')->getRealPath();

        $data = array_map('str_getcsv', file($path));

        if (count($data) > 0) {

            $csv_data = array_slice($data, 0, 2);

            $csv_data_file = CsvData::create([
                        'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
                        'csv_header' => false,
                        'csv_data' => json_encode($data)
            ]);
        } else {
            return redirect()->back();
        }

        $this->processImport($csv_data_file->id);
        return redirect('numbers');
    }

    public function processImport($csv_data_file_id) {

        $data = CsvData::find($csv_data_file_id);
        $csv_data = json_decode($data->csv_data, true);
        $account_id = Auth::user()->id;
        foreach ($csv_data as $row) {

            $topup_number = new Topup_number();

            $group_name = $row[0];
            $telco = $row[1];
            $mobile = $row[2];
            $amount = $row[3];
            $scheduled = $row[4];
            $frequency = $row[5];

            $exists = Topup_number::where('account_id', $account_id)->where('msisdn', $mobile)->first();

            if ($exists != null) {

                return;
            }

            //$group_id = 0;
            $group = Group::where('group_name', $group_name)->where('account_id', $account_id)->first();
            if ($group == null) {

                Group::create(['account_id' => $account_id, 'group_name' => $group_name]);
                $group_id = Group::where('group_name', $group_name)->where('account_id', $account_id)->first()->id;
            } else {
                $group_id = $group->id;
            }

            $topup_number->account_id = $account_id;
            $topup_number->group_id = $group_id;
            $topup_number->amount = $amount;
            $topup_number->is_scheduled = $scheduled;
            $topup_number->msisdn = $mobile;
            $topup_number->telco = $telco;
            $topup_number->frequency = $frequency;

            $topup_number->save();
        }
    }

}
