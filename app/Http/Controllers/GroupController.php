<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Group;

use Auth;
use Session;

class GroupController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $perPage = 5; 
        
        $id = Auth::user()->id;
        
        $groups = Group::where('account_id', $id)->orderBy('id', 'DESC')->paginate($perPage); 
        // dd($id);
        // echo $id;
        // die();     
        return view('user.groups', compact('groups'));        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $requestData["account_id"] = $account_id;
        
        Group::create($requestData);    

        Session::flash('flash_message', 'Group Record added!');

        return redirect('groups');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return view('show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $perPage = 5;
        
        $account_id = Auth::user()->id;
//        $account_id = 1;
        
        $groups = Group::where('account_id', $account_id)
                ->orderBy('id', 'DESC')->paginate($perPage);
        
        $group = Group::findOrFail($id);
        $submitButtonText = 'Update';
        
        return view('user.groups', compact('group', 'submitButtonText',
                'groups'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $group = Group::findOrFail($id);
        $group->update($requestData);

        Session::flash('flash_message', 'Group record updated!');
        
        return redirect('groups');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {       
        $id = $request->group_id;
        UserEducation::destroy($id);

        Session::flash('flash_message', 'Group record deleted!');

        return redirect('groups');
    }
    
}
