<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use App\Account;
use Session;
use Auth;

class ClientController extends Controller {

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('index');
    }

    public function contact_us() {
        return view('layouts.partials.contact_us');
    }

    public function register() {
        return view('auth.register');
    }

    public function about() {

        return view('layouts.partials.about');
    }

    public function login() {
        return view('client::auth.login');
    }

    public function confirm_account($confirmation) {

        if (User::where('confirmation_token', $confirmation)->exists()) {
            User::where('confirmation_token', $confirmation)->update([

                'active' => 1,
                'confirmation_token' => NULL
            ]);

            Session::flash('flash_message', 'Successful, Please  login  to contine');
            return redirect('client/login');
        } else {
            // wrong  code

            Session::flash('flash_message', 'The confirmation link is invalid');
            return redirect('client/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('client::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('client::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('client::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
        
    }

    public function sendmessage(Request $request) {

        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $message = $request->message;

        $adminemail = env('ADMIN_EMAIL', 'antonjoro2008@gmail.com');

        $user = new User();
        $user->email = $adminemail;

        $user->notify(new ContactusNotification($user, $name, $email, $phone, $message));

        Session::flash('flash_message', "Your message has been sent successfully. Our customer"
                . " service department will get back at you shortly. Thank you.");

        return view('client::layouts.partials.contact_us');
    }
    
    public function register_user(Request $request){
        
        $user = new User();

        $validatedData = $request->validate([
            'name'=>'required|max:55',
            'phone'=>'required|max:10|min:10|unique:users',
            'password'=>'required|min:6',
        ]);
        $validatedData['password'] = bcrypt($request->password);
         $user = User::create($validatedData);
        
        Session::flash("You have been registered successfully. Please login");
        
        return redirect('login');
    }
    
    
    public function login_user(Request $request){
        
        $username = $request->phone;
        $password = bcrypt($request->password);
        
        $user = User::where('email', $username)->where('password',
                $password);
        
        Auth::login($user);
        
        return redirect('profile');
//        return $user;
    }

}
