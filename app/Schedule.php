<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    //
    
    protected $primaryKey = 'id';
    protected $table = 'schedules';
    protected $fillable = ['group_id', 'frequency', 'day_of_week',
        'day_of_month', 'account_id'];

    public function group() {
        
        return $this->BelongsTo('App\Group');
    }
}