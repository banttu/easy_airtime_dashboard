<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topup_number extends Model {

    //

    protected $primaryKey = 'id';
    protected $table = 'topup_numbers';
    protected $fillable = ['group_id', 'account_id', 'amount',
        'is_scheduled', 'frequency', 'msisdn', 'telco'];

    public function group() {
        return $this->BelongsTo('App\Group', 'group_id', 'id');
    }

}
