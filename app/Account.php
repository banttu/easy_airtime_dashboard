<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $primaryKey = 'id'; 
    
    protected $table = 'accounts';
       
    protected $fillable = ['name', 'logo', 'physical_address', 
        'postal_address', 'postal_code', 'country_id', 'city', 'telephone', 
        'email_address', 'website', 'payment_plan', 'status'];
    
}
