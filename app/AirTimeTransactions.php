<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AirTimeTransactions extends Model
{
    protected $primaryKey = 'id'; 

    
    protected $table = 'airtime_transactions';
       
    protected $fillable = ['map_id','schedule_id','amount'];


    public function mappings(){
        return $this->hasMany(ContactMapping::class,'map_id', 'id');
    }

    public function schedules(){
        return $this->hasMany(Schedules::class,'schedule_id', 'id');
    }
 }
