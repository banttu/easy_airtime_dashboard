<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airtime_log extends Model
{
    //
	protected $primaryKey = 'id'; 
    
    protected $table = 'airtime_logs';
       
    protected $fillable = ['topup_number_id', 'msisdn', 'amount', 
        'status'];
}
