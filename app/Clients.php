<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContactGroups;

class Clients extends Model
{
    protected $primaryKey = 'id'; 
    
    protected $table = 'clients';
       
    protected $fillable = ['name'];

    // public function groups(){
    //     return $this->hasMany(ContactGroups::Class,'id','contact_id');
    // }

}
