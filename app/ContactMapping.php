<?php

namespace App;

use App\Contacts;

use Illuminate\Database\Eloquent\Model;

use  App\ContactGroups;

class ContactMapping extends Model
{
    protected $primaryKey = 'id'; 
    
    protected $table = 'contact_mapping';
       
    protected $fillable = ['contact_id','group_id','amount'];
    

public function contact(){
    return $this->hasOne('App\Contacts', 'id', 'contact_id');
}

  public function group(){
    return $this->hasOne(ContactGroups::class,'id', 'group_id');
}

// public function contacts(){
//     return $this->hasOne(Contacts::class,'id','contact_id');
// }


public function contacts(){
    return $this->hasMany(Contacts::class,'id', 'contact_id');
}

}
