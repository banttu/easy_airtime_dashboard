<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// use App\ContactGroups;

class Schedules extends Model
{
    protected $primaryKey = 'id'; 
    
    protected $table = 'schedules';
       
    protected $fillable = ['group_id','status','interval_value','interval_field','last_executed','next_run','starts_at','ends_at'];

    public function groups(){
        return $this->BelongsTo(ContactGroups::class,'group_id','id');
    }
}
