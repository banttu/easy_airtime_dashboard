<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirtimeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airtime_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('schedule_id');
            $table->bigInteger('map_id');
            $table->string('reference',256);
            $table->enum('status',['unprocessed','processed','processing','manual_confirmation','sent','failed','seccessful']);
            $table->bigInteger('send_time');
            $table->string('status_desc', 256);
            $table->integer('amount');
            $table->timestamps();
            $table->unique(['schedule_id','send_time', 'map_id']);
            $table->index('map_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airtime_transactions');
    }
}
