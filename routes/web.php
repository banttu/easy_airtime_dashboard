<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth','admin']], function (){

        Route::get('/dashboard','Admin\DashboardController@index');
        Route::get('/role-delete/{id}','Admin\DashboardController@registerdelete');
        Route::get('/user-add','Admin\DashboardController@create_user');
        Route::post('/create-user','Admin\DashboardController@store_user');
        Route::get('/groups','Admin\DashboardController@groups');
        Route::get('/clients','Admin\DashboardController@clients');
        Route::get('/group-add','Admin\DashboardController@addgroup');
        Route::get('/client-add','Admin\DashboardController@clientadd');
        Route::post('/add-client','Admin\DashboardController@addclient');
        Route::post('/add-group','Admin\DashboardController@groupadd');
        Route::get('/viewclient/{id}','Admin\DashboardController@viewclient');
        Route::post('/update-client/{id}','Admin\DashboardController@updateclient');
        Route::get('editgroup/{id}','Admin\DashboardController@editGroup');
        Route::post('update-group/{id}','Admin\DashboardController@updateGroup');
        Route::get('/contacts','Admin\DashboardController@contacts');
        Route::get('/contact-add','Admin\DashboardController@addcontact');
        Route::post('/add-contact','Admin\DashboardController@createcontact');
        Route::get('/editcontact/{id}','Admin\DashboardController@editcontact');
        Route::post('/update-contact/{id}','Admin\DashboardController@updateContact');
        Route::get('/mapping','Admin\DashboardController@mapping');
        Route::get('/mapping-add','Admin\DashboardController@addmapping');
        Route::get ('/mapcontact/{id}','Admin\DashboardController@mappcontact');
        Route::post ('/contactMap','Admin\DashboardController@contactMap');
        Route::get('/map-edit/{id}','Admin\DashboardController@mapedit');
        Route::post('/editMap/{id}','Admin\DashboardController@editmap');
        Route::get('/viewschedules/{id}','Admin\DashboardController@schedules');
        Route::get('/schedules-add','Admin\DashboardController@addschedule');
        Route::get('/viewgroup/{id}','Admin\DashboardController@viewcontacts');
        Route::get('/schedulesedit/{id}','Admin\DashboardController@editschedule');
        Route::post('/scheduleupdate/{id}',"Client\ClientController@updateschedule");
        Route::post('/scheduleupdate/{id}','Admin\DashboardController@saveschedule');
        Route::get('/schedule-create/{id}','Admin\DashboardController@schedulecreate');
        Route::get('/transactions','Admin\DashboardController@transactions');
    });


    Route::group(['middleware' => ['auth']], function (){
        Route::get('/client','Client\ClientController@index');
        Route::get('/client-user-add','Client\ClientController@create_user');
        Route::post('/client-create-user','Client\ClientController@store_user');
        Route::get('/client-groups','Client\ClientController@groups');
        Route::get('/client-group-add','Client\ClientController@addgroup');
        Route::post('/client-add-client','Client\ClientController@addclient');
        Route::post('/client-add-group','Client\ClientController@groupadd');
        Route::get('/client-viewclient/{id}','Client\ClientController@viewclient');
        Route::post('/client-update-client/{id}','Client\ClientController@updateclient');
        Route::get('/client-editgroup/{id}','Client\ClientController@editGroup');
        Route::post('client-update-group/{id}','Client\ClientController@updateGroup');
        Route::get('/client-contacts','Client\ClientController@contacts');
        Route::get('/addContact/{id}','Client\ClientController@addcontact');
        Route::post('/client-add-contact','Client\ClientController@createcontact');
        Route::get('/client-editcontact/{id}','Client\ClientController@editcontact');
        Route::post('/client-update-contact/{id}','Client\ClientController@updateContact');
        Route::get('/client-mapping','Client\ClientController@mapping');
        Route::get('/client-mapping-add','Client\ClientController@addmapping');
        Route::get ('/client-mapcontact/{id}','Client\ClientController@mappcontact');
        Route::post ('/client-contactMap','Client\ClientController@contactMap');
        Route::get('/client-map-edit/{id}','Client\ClientController@mapedit');
        Route::post('/client-editMap/{id}','Client\ClientController@editmap');
        Route::get(' /client-group/{id}','Client\ClientController@schedules');
        Route::get('/client-schedules-add/{id}','Client\ClientController@addschedule');
        Route::post('/saveschedule/{id}','Client\ClientController@saveschedule');
        Route::get('/client-group/editschedules/{id}',"Client\ClientController@editschedules");
        Route::post('/updateschedule/{id}',"Client\ClientController@updateschedule");
        Route::get('/client-viewgroup/{id}','Client\ClientController@viewcontacts')->name('viewcontacts');
        Route::get('/client-schedule-create/{id}','Client\ClientController@schedulecreate');
        Route::get('/client-transactions','Client\ClientController@transactions');
        Route::post('csv_file/import', 'CsvFile@csv_import')->name('import');
        Route::get('/contacts/download/{file}', 'Client\ClientController@getDownload');
    });
   