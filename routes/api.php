<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Authentication
Route::post('/register', 'ApiAuthController@register');
Route::post('/login', 'ApiAuthController@login');
Route::group([
    'middleware' => 'auth:api'
], function () {

//Profile
Route::get('/profile', 'ApiController@profile');
Route::post('update-profile', 'ApiController@updateProfile');

//Airtime
Route::get('airtimeLogs', 'ApiController@transactions');

//Groups
Route::get('groups', 'ApiController@groups');
Route::post('create-group', 'ApiController@creategroup');
Route::post('update-group/{id}', 'ApiController@updategroup');
Route::get('show-group/{id}', 'ApiController@showgroup');
Route::get('delete-group/{id}', 'ApiController@deletegroup');

//Contacts
Route::post('add-number', 'ApiController@storeContact');
Route::post('update-number/{id}', 'ApiController@updateContact');
Route::post('delete-number/{id}', 'ApiController@destroyContact');

//Schedules
Route::post('add-schedule/{id}', 'ApiController@storeSchedule');
Route::post('update-schedule/{id}', 'ApiController@updateSchedule');
Route::get('delete-schedule/{id}', 'ApiController@destroySchedule');
});